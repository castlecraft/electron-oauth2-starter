import { stringify } from 'querystring';
import * as electron from 'electron';
import { parse } from 'url';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as keyTar from 'keytar';
import { Injectable } from '@angular/core';
import {
  ACCESS_TOKEN,
  ELECTRON,
  REFRESH_TOKEN,
  ID_TOKEN,
  SCOPE,
  TOKEN_TYPE,
} from '../../../constants/storage';

export interface OAuth2Config {
  redirectUri: string;
  clientId: string;
  scope: string;
  authorizationUrl: string;
}

export interface OAuth2UrlParams {
  response_type?: string;
  redirect_uri?: string;
  client_id?: string;
  state?: string;
  scope?: string;
  access_type?: string;
}

export interface GetCodeOpts {
  scope?: string;
  accessType?: string;
  additionalAuthCodeRequestData?: any;
}

@Injectable()
export class OAuth2Service {
  config: OAuth2Config;
  windowParams: any;
  authorizationHeaders: HttpHeaders;
  tokenRequestUrl = 'https://staging-accounts.castlecraft.in/oauth2/';
  constructor(private readonly http: HttpClient) {}

  setConfig(config: OAuth2Config, windowParams: any) {
    this.config = config;
    this.windowParams = windowParams;
  }

  getAuthorizationCode(opts: GetCodeOpts) {
    opts = opts || {};
    let urlParams: OAuth2UrlParams = {
      response_type: 'code',
      redirect_uri: this.config.redirectUri,
      client_id: this.config.clientId,
      state: this.generateRandomString(16),
    };

    if (opts.scope) {
      urlParams.scope = opts.scope;
    }

    if (opts.accessType) {
      urlParams.access_type = opts.accessType;
    }

    urlParams = Object.assign(urlParams, opts.additionalAuthCodeRequestData);

    const url = this.config.authorizationUrl + '?' + stringify(urlParams);
    const self = this;
    return new Promise((resolve, reject) => {
      const authWindow = new electron.remote.BrowserWindow(
        self.windowParams || { 'use-content-size': true },
      );
      authWindow.loadURL(url);
      authWindow.show();

      authWindow.on('closed', () => {
        reject(new Error('window was closed by user'));
      });

      function onCallback(callbackUrl) {
        const urlParts = parse(callbackUrl, true);
        const query = urlParts.query;
        const code = query.code;
        const state = query.state;
        const error = query.error;

        if (error !== undefined) {
          reject(error);
          authWindow.removeAllListeners('closed');
          setImmediate(() => {
            authWindow.close();
          });
        } else if (code) {
          resolve({ code, state });
          authWindow.removeAllListeners('closed');
          setImmediate(() => {
            authWindow.close();
          });
        }
      }

      authWindow.webContents.on('will-navigate', (event, Url) => {
        onCallback(Url);
      });

      authWindow.webContents.on('will-redirect', (event, oldUrl, newUrl) => {
        onCallback(oldUrl);
      });
    });
  }

  getNewToken(verifier) {
    this.http.post(this.tokenRequestUrl + 'token', verifier).subscribe({
      next: (response: any) => {
        this.setLocalSecrets(response);
        keyTar
          .findCredentials(ELECTRON)
          .then(data => {})
          .catch(err => {});
      },
      error: err => {},
    });
  }

  getToken() {
    return keyTar
      .getPassword(ELECTRON, ACCESS_TOKEN)
      .then(token => {
        return token;
      })
      .catch(err => {
        return err;
      });
  }

  async getRefreshToken(verifier) {
    const oldToken = await this.getToken();
    keyTar
      .getPassword(ELECTRON, REFRESH_TOKEN)
      .then(value => {
        verifier.refresh_token = value;
        this.http.post(this.tokenRequestUrl + 'token', verifier).subscribe({
          next: (data: any) => {
            this.http
              .post(
                this.tokenRequestUrl + 'revoke',
                { token: oldToken },
                { headers: this.getHeaders(data.access_token) },
              )
              .subscribe({
                next: response => {
                  this.updateSecrets(data);
                  // just to log our ketTar passwords.
                  keyTar
                    .findCredentials(ELECTRON)
                    .then(credentials => {})
                    .catch(err => {});
                },
                error: err => {},
              });
          },
          error: err => {},
        });
      })
      .catch(err => {});
  }

  generateRandomString(length: number) {
    let text = '';
    const possible =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  setLocalSecrets(response) {
    keyTar.setPassword(ELECTRON, ACCESS_TOKEN, response.access_token);
    keyTar.setPassword(ELECTRON, REFRESH_TOKEN, response.refresh_token);
    keyTar.setPassword(ELECTRON, ID_TOKEN, response.id_token);
    keyTar.setPassword(ELECTRON, SCOPE, response.scope);
    keyTar.setPassword(ELECTRON, TOKEN_TYPE, response.token_type);
  }

  updateSecrets(response) {
    keyTar.setPassword(ELECTRON, ACCESS_TOKEN, response.access_token);
    keyTar.setPassword(ELECTRON, REFRESH_TOKEN, response.refresh_token);
  }

  getHeaders(token) {
    return (this.authorizationHeaders = new HttpHeaders({
      Authorization: 'Bearer ' + token,
    }));
  }
}
