import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { MatSnackBar } from '@angular/material';
import { OAuth2Service } from './common/services/oauth2/oauth2.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'accounting-desktop';
  loggedIn = true;
  data: string;
  constructor(
    private readonly appService: AppService,
    private readonly snackBar: MatSnackBar,
    private readonly oauth2: OAuth2Service,
  ) {}
  ngOnInit() {
    this.appService.getMessage().subscribe({
      next: response => {
        if (response.message) {
          this.snackBar.open('Server Setup Incomplete', 'Close', {
            duration: 2500,
          });
        }
        this.data = JSON.stringify(response);
        this.oauth2.setConfig(
          {
            redirectUri: 'http://localhost/callback',
            clientId: response.clientId,
            scope: 'openid email roles',
            authorizationUrl: response.authorizationURL,
          },
          { width: 380, height: 635 },
        );
      },
    });
  }

  getRefreshToken() {
    this.appService.getRefreshToken();
    this.loggedIn = false;
  }

  login() {
    this.appService.oAuthLogin();
    this.loggedIn = false;
  }
}
